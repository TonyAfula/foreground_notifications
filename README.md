
### About
Foreground reminder is an Foreground service Android Application that displays floating notifications(overlay window) periodically

#### Prerequisites
Before running this application, ensure you have [Android Studio IDE](https://developer.android.com/studio) installed.



#### Installation
1. Import the project in Android Studio IDE using **File->New->Import Project Menu**.


2. Run the application using either an emulator or android device using 'Run' command.
   [Read more...](https://developer.android.com/training/basics/firstapp/running-app)

### Modifying the source code
1. To change the time of notification change **NOTIFICATION_FREQUENCY_MIN** value in
    com.shalommedia.foregroundnotifications.helpers.AlarmHelper to a value in minutes.

    ```
      public static final int NOTIFICATION_FREQUENCY_MIN = 1;
    ```

2. To change the icon of the floating notification change the value of **android:background** attribute of
    floating_view_background ImageView in view_floating_view.xml layout file.

    ```
        <ImageView
             android:id="@+id/floating_view_background"
             android:layout_width="@dimen/floating_view_size"
             android:layout_height="@dimen/floating_view_size"
             android:layout_alignParentStart="true"
             android:layout_alignParentLeft="true"
             android:layout_alignParentTop="true"
             android:background="@drawable/floating_notifications" />
    ```

3. To change the message and title of the foreground notification,
  change the **title** and **messageToShow** variables
  in com.shalommedia.foregroundnotifications.services.ForegroundService#onStartCommand method.

    ```
        String title = context.getResources().getString(R.string.app_name);
        String messageToShow = "This is a test message";
    ```


4. To change the custom tab urls, change the value of the following fields
  in com.shalommedia.foregroundnotifications.utils.ChromeTabUtils class.

    ```
       public static final String URL_1= "https://google.com#url1";
       public static final String URL_2= "https://yahoo.com#url2";
    ```