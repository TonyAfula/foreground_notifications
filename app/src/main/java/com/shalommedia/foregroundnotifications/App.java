package com.shalommedia.foregroundnotifications;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.shalommedia.foregroundnotifications.helpers.NotificationHelper;

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        NotificationHelper notificationHelper = new NotificationHelper(this);
        notificationHelper.createNotificationChannel();
    }


}

