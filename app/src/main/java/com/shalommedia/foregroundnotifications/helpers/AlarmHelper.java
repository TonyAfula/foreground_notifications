/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shalommedia.foregroundnotifications.helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import com.shalommedia.foregroundnotifications.receivers.AlarmTriggerReceiver;
import com.shalommedia.foregroundnotifications.receivers.BootReceiver;

import java.util.concurrent.TimeUnit;

public class AlarmHelper {
    private  String TAG= "AlarmHelper";
    private AlarmManager alarmManager;
    /* Notification frequency in minutes */
    public static final int NOTIFICATION_FREQUENCY_MIN = 1;//

    private final static String ACTION_BD_NOTIFICATION = "com.shalommedia.foregroundnotifications.NOTIFICATION";

    public void setAlarm(Context context) {
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);


        PendingIntent pendingAlarmIntent=getPendingIntent(context);
        //Log.i(TAG, "Setting Alarm Interval to: " + NOTIFICATION_FREQUENCY_MIN + " minutes");


        long currentMillis = System.currentTimeMillis();
        long notificationFrequencyMs  = TimeUnit.MINUTES.toMillis(NOTIFICATION_FREQUENCY_MIN);
        long triggerAt= currentMillis+notificationFrequencyMs;


        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                triggerAt,
                notificationFrequencyMs,
                pendingAlarmIntent);

        /* Restart if rebooted */
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        context.getPackageManager().setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void cancelAlarm(Context context) {
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        PendingIntent pendingAlarmIntent=getPendingIntent(context);
        alarmManager.cancel(pendingAlarmIntent);

        /* Alarm won't start again if device is rebooted */
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    private PendingIntent getPendingIntent(Context context){

        Intent alarmIntent = new Intent(context, AlarmTriggerReceiver.class);
        alarmIntent.setAction(ACTION_BD_NOTIFICATION);

        PendingIntent pendingAlarmIntent = PendingIntent.getBroadcast(context,
                0,
                alarmIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);


        return pendingAlarmIntent;
    }
}
