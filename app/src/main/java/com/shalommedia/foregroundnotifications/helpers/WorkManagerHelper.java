package com.shalommedia.foregroundnotifications.helpers;

import android.content.Context;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.shalommedia.foregroundnotifications.services.PopupWorker;

import java.util.concurrent.TimeUnit;

public class WorkManagerHelper {
    public static final int NOTIFICATION_FREQUENCY_MIN = 15;//

    public  void schedule(Context context) {

        Constraints constraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        //The minimum repeat interval that can be defined is 15 minutes (same as the JobScheduler API).
        PeriodicWorkRequest saveRequest =
                new PeriodicWorkRequest.Builder(PopupWorker.class, NOTIFICATION_FREQUENCY_MIN, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .build();

        WorkManager.getInstance(context)
                .enqueue(saveRequest);


    }
}
