/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shalommedia.foregroundnotifications.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.shalommedia.foregroundnotifications.services.PopupService;
import com.shalommedia.foregroundnotifications.ui.MainActivity;

import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_2;
import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_EXTRA;
//https://medium.com/@Chanddru/schedule-tasks-with-alarm-manager-in-modern-android-devices-%EF%B8%8F-1b95bb5e335
public class AlarmTriggerReceiver extends BroadcastReceiver {
    private String TAG = "AlarmTriggerReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        // On Oreo+, it is required to use startForegroundService
        // with implicit promise to start a foreground notification
        // within the ANR timeout. Popup notification
        Intent popupService = new Intent(context, PopupService.class);
        context.stopService(popupService);
        //intent.putExtra("command", "notif"+notifNumber);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(popupService);
        } else {
            context.startService(popupService);
        }

        Intent mainIntent = new Intent(context, MainActivity.class);
        mainIntent.putExtra(URL_EXTRA, URL_2);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    }




}
