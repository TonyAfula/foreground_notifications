package com.shalommedia.foregroundnotifications.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.shalommedia.foregroundnotifications.R;
import com.shalommedia.foregroundnotifications.ui.MainActivity;

import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_2;
import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_EXTRA;


public class ForegroundService extends Service {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String title = getResources().getString(R.string.app_name);
        String messageToShow = "This is a test message";


        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.putExtra(URL_EXTRA, URL_2);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent =
                PendingIntent.getActivity(this, 0, mainIntent, 0);
        /* Notify */


        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(messageToShow)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(contentIntent)
                .build();

        startForeground(51, notification);

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
