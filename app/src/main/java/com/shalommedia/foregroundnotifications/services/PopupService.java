package com.shalommedia.foregroundnotifications.services;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.shalommedia.foregroundnotifications.R;
import com.shalommedia.foregroundnotifications.ui.MainActivity;

import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_2;
import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_EXTRA;


public class PopupService extends Service {
    private  String TAG= "PopupService";
    private WindowManager windowManager;
    private RelativeLayout mFloatingView;
    private TextView mBadgeTextView;
    private WindowManager.LayoutParams mFloatingParams;
    private LayoutInflater mLayoutInflater;


    @Override
    public void onCreate() {
        super.onCreate();


        Log.i(TAG, "onCreate()....");

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        //setAsForeground();


        mLayoutInflater = LayoutInflater.from(this);
        mFloatingView = (RelativeLayout) mLayoutInflater.inflate(R.layout.view_floating_view, null);
        mBadgeTextView = mFloatingView.findViewById(R.id.floating_view_badge);
        mBadgeTextView.setVisibility(View.VISIBLE);
        mBadgeTextView.setText(Integer.toString(1));


        mFloatingParams = buildParams(Gravity.TOP|Gravity.LEFT, 0, 100);

        //this code is for dragging the popup notification
        mFloatingView.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            /**
             * Number of milliseconds below which a motion is considered a single tap
             */
            private long mSingleTapThreshold = 180;
            private long mLastDown = 0;

            private boolean isSingleTap() {
                return System.currentTimeMillis() - mLastDown <= mSingleTapThreshold;
            }

            private boolean mLastIsFloatingOverlapDismiss = false;

            private boolean floatingOverlapsDismiss() {
                int[] headPos = new int[2];
                int[] dismissPos = new int[2];

                mFloatingView.getLocationOnScreen(headPos);
                mDismissView.getLocationOnScreen(dismissPos);

                // Rect constructor parameters: left, top, right, bottom
                Rect headRect = new Rect(headPos[0],
                        headPos[1],
                        headPos[0]+mFloatingView.getMeasuredWidth(),
                        headPos[1]+mFloatingView.getMeasuredHeight());
                Rect dismissRect = new Rect(dismissPos[0],
                        dismissPos[1],
                        dismissPos[0]+mDismissView.getMeasuredWidth(),
                        dismissPos[1]+mDismissView.getMeasuredHeight());
                return headRect.intersect(dismissRect);
            }

            private boolean isOnLeftHalfOfScreen(int x) {
                return x < (getScreenWidth() / 2);
            }

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        setDismissViewVisibleAnimated(true, null);
                        initialX = mFloatingParams.x;
                        initialY = mFloatingParams.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        mLastDown = System.currentTimeMillis();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if(floatingOverlapsDismiss()) {
                            animatedStopSelf();
                            return true;
                        }

                        setDismissViewVisibleAnimated(false, null);

                        if (isSingleTap()) {
                            openUrl2();
                        }

                        // Nothing else: stick floater to screen edge
                        if (isOnLeftHalfOfScreen(mFloatingParams.x)) {
                            stickFloaterToLeftEdge(true);
                        } else {
                            stickFloaterToLeftEdge(false);
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        mFloatingParams.x = initialX
                                + (int) (event.getRawX() - initialTouchX);
                        mFloatingParams.y = initialY
                                + (int) (event.getRawY() - initialTouchY);

                        try {

                            windowManager.updateViewLayout(mFloatingView, mFloatingParams);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }


                        // If overlap state changes
                        if(mLastIsFloatingOverlapDismiss != floatingOverlapsDismiss()) {
                            // And is changing from not overlapping to overlapping (entering)
                            if(!mLastIsFloatingOverlapDismiss && floatingOverlapsDismiss()) {
                                vibrate();
                            }
                        }
                        mLastIsFloatingOverlapDismiss = floatingOverlapsDismiss();

                        if (isOnLeftHalfOfScreen(mFloatingParams.x)) alignBadgeLeft(false);
                        else alignBadgeLeft(true);
                        return true;
                }
                return false;
            }
        });
        windowManager.addView(mFloatingView, mFloatingParams);
        setupDismissView();

        setFloatingViewVisibleAnimated(true, null);


    }




    //region Dismiss View
    private RelativeLayout mDismissView;
    private WindowManager.LayoutParams mDismissParams;
    private void setupDismissView() {
        mDismissView = (RelativeLayout) mLayoutInflater.inflate(R.layout.view_dismiss_view, null);
        mDismissParams = buildParams(Gravity.CENTER | Gravity.BOTTOM, 0, 0);
        mDismissView.setAlpha(0);
        mDismissView.setVisibility(View.GONE);
        windowManager.addView(mDismissView, mDismissParams);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    private void animatedStopSelf() {
        // Unexpand if expanded view is expanded,
        // animate floater view away,
        // then on animation end, animate dismiss view away,
        // then on animation end, stop service.
        setFloatingViewVisibleAnimated(false, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setDismissViewVisibleAnimated(false, new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        stopSelf();
                    }
                });
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unsetAsForeground();
        removeViews();

    }

    //region Foregrounding
    private static final int FOREGROUND_ID = 15487;
    private void setAsForeground() {

        NotificationCompat.Builder notif;

        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.putExtra(URL_EXTRA, URL_2);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        PendingIntent appPending =
                PendingIntent.getActivity(this, 0, mainIntent, 0);

        // Set up notification, handling Android O's requirement for a channel ID
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notif = new NotificationCompat.Builder(this)
                    .setContentTitle(getText(R.string.floater_title))
                    .setContentText(getText(R.string.floater_description))
                    .setContentIntent(appPending);
        }
        else {
            notif = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                    .setContentTitle(getText(R.string.floater_title))
                    .setContentText(getText(R.string.floater_description))
                    .setContentIntent(appPending)
                    .setPriority(NotificationCompat.PRIORITY_LOW);
        }

        notif.setSmallIcon(R.drawable.ic_stat_local_drink);
        notif.setAutoCancel(true);

        startForeground(FOREGROUND_ID, notif.build());
    }

    private void unsetAsForeground() {
        stopForeground(true);
    }

    /**
     * Align badge to the left
     *
     * @param toLeft if true, align to left. If false, to right.
     */
    private void alignBadgeLeft(boolean toLeft) {
        RelativeLayout.LayoutParams badgeParams = (RelativeLayout.LayoutParams) mBadgeTextView.getLayoutParams();
        badgeParams.removeRule(RelativeLayout.ALIGN_END);
        badgeParams.removeRule(RelativeLayout.ALIGN_START);

        if (toLeft)
            badgeParams.addRule(RelativeLayout.ALIGN_START, R.id.floating_view_background);
        else
            badgeParams.addRule(RelativeLayout.ALIGN_END, R.id.floating_view_background);

        mBadgeTextView.setLayoutParams(badgeParams);
    }

    private void removeView(View view) {
        windowManager.removeView(view);
    }

    private void removeViews() {
        removeView(mFloatingView);
        removeView(mDismissView);

    }

    /**
     * @param toLeft true, stick floater to left edge. False, to right edge.
     */
    private void stickFloaterToLeftEdge(boolean toLeft) {
        int currentX = mFloatingParams.x;
        int targetX;
        if (toLeft) targetX = 0;
        else targetX = getScreenWidth();

        ValueAnimator va = ValueAnimator.ofInt(currentX, targetX);
        va.setDuration(100);
        va.addUpdateListener(animation -> {
            mFloatingParams.x = (int) animation.getAnimatedValue();
            try {
                windowManager.updateViewLayout(mFloatingView, mFloatingParams);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
        va.start();
    }

    private WindowManager.LayoutParams buildParams(int gravity, int startX, int startY) {
        WindowManager.LayoutParams params;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT);
        }
        params.gravity = gravity;
        params.x = startX;
        params.y = startY;
        return params;
    }

    private DisplayMetrics getScreenMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    private int getScreenWidth() {
        return getScreenMetrics().widthPixels;
    }

    private int getScreenHeight() {
        return getScreenMetrics().heightPixels;
    }

    private Vibrator mVibrator;
    private void vibrate() {
        mVibrator.vibrate(500);
    }

    private void markBadgeRead() {
        setBadgeCount(0);
    }

    private void setBadgeCount(int count) {
        mBadgeTextView.setText(Integer.toString(count));
        if (count == 0) mBadgeTextView.setVisibility(View.INVISIBLE);
        else mBadgeTextView.setVisibility(View.VISIBLE);
    }

    private void openUrl2() {
        markBadgeRead();
        setFloatingViewVisibleAnimated(false, null);

        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.putExtra(URL_EXTRA, URL_2);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainIntent);
        stopSelf();
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Animate the visibility of the Dismiss view
     * @param makeVisible if true, animate the Dismiss View into sight.
     *                  if false, animate the Dismiss View out of sight.
     */
    private void setDismissViewVisibleAnimated(final boolean makeVisible, @Nullable AnimatorListenerAdapter listener) {
        int invisibleY = 0; float invisibleA = 0;
        int visibleY = 100;
        float visibleA = 1;
        int currentY, targetY; float currentA, targetA;
        if(makeVisible) {
            currentY = invisibleY; targetY = visibleY;
            currentA = invisibleA; targetA = visibleA;
        }
        else {
            currentY = visibleY; targetY = invisibleY;
            currentA = visibleA; targetA = invisibleA;
        }

        // Translation
        ValueAnimator va = ValueAnimator.ofInt(currentY, targetY);
        va.setDuration(100);
        va.addUpdateListener(animation -> {
            mDismissParams.y = (int) animation.getAnimatedValue();
            try {
                windowManager.updateViewLayout(mDismissView, mDismissParams);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });

        // Alpha
        ValueAnimator tva = ValueAnimator.ofFloat(currentA, targetA);
        tva.setDuration(100);
        tva.addUpdateListener(animation -> mDismissView.setAlpha((float) animation.getAnimatedValue()));

        va.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if(makeVisible) mDismissView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(!makeVisible) mDismissView.setVisibility(View.GONE);
            }
        });

        if(listener != null) tva.addListener(listener);

        va.start();
        tva.start();
    }

    /**
     * @param makeVisible true means floater will be visible at the end of the animation,
     *                    false means floater will be gone at the end of the animation
     */
    private void setFloatingViewVisibleAnimated(final boolean makeVisible, @Nullable AnimatorListenerAdapter animationListener) {
        // Animate alpha
        float visibleAlpha = 1, invisibleAlpha = 0;
        float currentAlpha, targetAlpha;
        if (makeVisible) {
            currentAlpha = invisibleAlpha;
            targetAlpha = visibleAlpha;
        } else {
            currentAlpha = visibleAlpha;
            targetAlpha = invisibleAlpha;
        }

        ValueAnimator va = ValueAnimator.ofFloat(currentAlpha, targetAlpha);
        va.setDuration(100);
        va.addUpdateListener(animation -> mFloatingView.setAlpha((float) animation.getAnimatedValue()));

        va.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mFloatingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!makeVisible) mFloatingView.setVisibility(View.GONE);
            }
        });

        if (animationListener != null) va.addListener(animationListener);

        va.start();
    }


}
