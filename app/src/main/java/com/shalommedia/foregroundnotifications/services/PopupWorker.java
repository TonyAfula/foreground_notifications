package com.shalommedia.foregroundnotifications.services;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class PopupWorker extends Worker {
    private String TAG= "PopupWorker";


    private Context mContext;

    public  PopupWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);

        mContext = context;
    }


    @Override
    public Result doWork() {
        Log.i(TAG, "doing work ...");
        // On Oreo+, it is required to use startForegroundService
        // with implicit promise to start a foreground notification
        // within the ANR timeout. Popup notification
        Intent popupService = new Intent(mContext, PopupService.class);
        mContext.stopService(popupService);
        //intent.putExtra("command", "notif"+notifNumber);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mContext.startForegroundService(popupService);
        } else {
            mContext.startService(popupService);
        }

        // Indicate whether the task finished successfully with the Result
        return Result.success();
    }



}
