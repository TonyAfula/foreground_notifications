package com.shalommedia.foregroundnotifications.ui;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.shalommedia.foregroundnotifications.R;
import com.shalommedia.foregroundnotifications.helpers.AlarmHelper;
import com.shalommedia.foregroundnotifications.services.ForegroundService;
import com.shalommedia.foregroundnotifications.utils.ChromeTabUtils;

import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;
import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_1;
import static com.shalommedia.foregroundnotifications.utils.ChromeTabUtils.URL_EXTRA;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = "MainActivity";
    private ChromeTabUtils chromeTabUtils;
    private Button openUrlButton;
    private Button closeAppButton;
    //private FakeAppUpdateManager appUpdateManager;
    private AppUpdateManager appUpdateManager;
    private final int APP_UPDATE_REQUEST_CODE = 3818;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(this);
        //appUpdateManager = new FakeAppUpdateManager(this);

        chromeTabUtils = new ChromeTabUtils(this);

        openUrlButton = findViewById(R.id.openUrlButton);
        closeAppButton = findViewById(R.id.closeAppButton);

        openUrlButton.setOnClickListener(this);
        closeAppButton.setOnClickListener(this);


        startForegroundService();
        openURL();
        checkForUpdates();

    }



    public void startForegroundService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }


    private void openURL() {
        final Intent intent = getIntent();
        final String urlExtra = intent.getStringExtra(URL_EXTRA);
        String url = TextUtils.isEmpty(urlExtra) ? URL_1 : urlExtra;

        chromeTabUtils.openUrl(url);

        if (url == URL_1) {
            if (hasOverlayPermission()) {
                AlarmHelper alarm = new AlarmHelper();
                alarm.cancelAlarm(this);
                alarm.setAlarm(this);


//                WorkManagerHelper workManagerHelper =new WorkManagerHelper();
//                workManagerHelper.schedule(this);
            } else {
                Intent requestPermissionIntent = new Intent(this, RequestPermissionActivity.class);
                startActivity(requestPermissionIntent);
                finish();
            }
        }
    }



    private void checkForUpdates() {
        Log.i(TAG, "Checking for updates ...");
     // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)) {
                // Request the update.

                startUpdate(appUpdateInfo);
            }
        });
    }

    private void startUpdate(AppUpdateInfo appUpdateInfo) {
        try {
            Log.i(TAG, "Performing updates ...");
            appUpdateManager.startUpdateFlowForResult(
                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                    appUpdateInfo,
                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                    IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    APP_UPDATE_REQUEST_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    // Checks that the update is not stalled during 'onResume()'.
    // However, you should execute this check at all entry points into the app.
    @Override
    protected void onResume() {
        super.onResume();

        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                // If an in-app update is already running, resume the update.
                                startUpdate(appUpdateInfo);
                            }
                        });

//        closeApp();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APP_UPDATE_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Log.i(TAG, "Update flow failed! Result code: " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }



    public boolean hasOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {
            return Settings.canDrawOverlays(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.openUrlButton:
                chromeTabUtils.openUrl(URL_1);
                break;
            case R.id.closeAppButton:
                closeApp();
                break;
        }
    }

    private void closeApp() {
        moveTaskToBack(true);
        finish();
    }
}
