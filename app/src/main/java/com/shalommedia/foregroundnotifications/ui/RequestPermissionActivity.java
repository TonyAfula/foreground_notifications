package com.shalommedia.foregroundnotifications.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.shalommedia.foregroundnotifications.R;

public class RequestPermissionActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 19658;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_permission);

        findViewById(R.id.requestPermissionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, REQUEST_CODE);
                }
                else returnToMain();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if(!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, R.string.permission_not_granted, Toast.LENGTH_LONG).show();
                }
                else returnToMain();
            }
            else returnToMain();
        }
    }

    private void returnToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
